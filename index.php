<!DOCTYPE html>
<html><head>

        <meta charset="UTF-8">

        <title>EddMi Simulator</title>
        <script type="text/javascript" src="script.js"></script>
        <script type="text/javascript" src="jquery-1.9.1.js"></script>
        <link href='style.css' rel='stylesheet' type='text/css'>
        <style>
            body {
                margin: 0;
            }
            #result_div {
                background: white;
            }
            iframe {
                width: 100%;
                border: 0;
            }
        </style>

        <style type="text/css"></style></head>

    <body>
        <!-- Menu  left -->
        <div class="box">
            <div class="box-inner">
                <ul class="menu">
                    <li>
                        <a class="iphone" href="javascript:void(0)">
                            Mobile
                        </a>
                    </li>
                    <li>
                        <a class="tablet" href="javascript:void(0)">
                            Tablet
                        </a>
                    </li>
                    <li>
                        <a class="laptop" href="javascript:void(0)">
                            Laptop
                        </a>
                    </li>
                    <li>
                        <a class="desktop" href="javascript:void(0)">
                            Desktop
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <!-- search right -->
        <div class="box-url">
            <form>
                <input id="site" type="url" class="form-control" placeholder="http://">
                <button id="goTo">Testing site</button>
            </form>
        </div>


        <div class="container">
            <div class="device-cover">
                <div class="speaker-1"></div>
                <iframe class="device-front" width="320" height="480" frameborder="0" src="https://www.eddmi.com/login"></iframe>
                <div class="device-lbl"><img src="logo.png" /></div>
            </div><!--/.device-cover -->
        </div><!--/.container -->

    </body>
</html>
<script>
    /* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
(function() {

    'use-strict';

    // Init app
    $('.container').css({
        'max-width': '417px',
        height: '480px'
    });
    $('.device-front').css({
        'max-width': '320px',
        height: '480px'
    });


    // Iphone
    $('.iphone').on('click', function() {
        $('.container').animate({
            'max-width': '417px',
            height: '480px'
        }, 1000);
        $('.device-front').animate({
            'max-width': '320px',
            height: '480px'
        }, 1000);
    });

    // Tablet
    $('.tablet').on('click', function() {
        $('.container').animate({
            'max-width': '861px',
            height: '1024px'
        }, 1000);
        $('.device-front').animate({
            'max-width': '768px',
            height: '1024px'
        }, 1000);
    });

    // Laptop
    $('.laptop').on('click', function() {
        $('.container').animate({
            'max-width': '1297px',
            height: '800px'
        }, 1000);
        $('.device-front').animate({
            'max-width': '1200px',
            height: '800px'
        }, 1000);
    });

    // Desktop
    $('.desktop').on('click', function() {
        $('.container').animate({
            'max-width': '1497px',
            height: '800px'
        }, 1000);
        $('.device-front').animate({
            'max-width': '1400px',
            height: '800px'
        }, 1000);
    });

    // Change Url
    $('#goTo').on('click', function() {
        $(this).text('Loading..');
        setTimeout(function() {
            $('#goTo').text('Go to other');
        }, 2000);
        $('.device-front').attr('src', $('#site').val());
        return false;
    });
}).call(this);


</script>